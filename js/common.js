jQuery(document).ready(function(){

	$.validator.addMethod('integer', function(value, element, param) {
            return (value != 0) && (value == parseInt(value, 10));
        }, 'Please enter a non zero integer value!');
	
	jQuery("#loginForm").validate({
		errorElement:'div',
		
		errorPlacement: function (error, element) {
            error.insertAfter(element).position({
                my: 'left center',
                at: 'right center',
                of: element,
                offset: '5 0'
            });
        },
		
		rules: {
			username:{
				required:true,
				email: true
			},
			password:{
				required:true,
				minlength:'6'
			}
		},
		messages: {
			username:{
				required:"Please enter valid email address.",
				email:  "Please enter valid email address."
			},
			password:{
				required:  "Please provide a password.",
				minlength: "At least 6 characters long."
			}
		}
		
//  		success: function(label){
//  			label.hide();
//  		}
	});	
	
					
	jQuery("#singupForm").validate({
		errorElement:'div',
		errorPlacement: function (error, element) {
            error.insertAfter(element).position({
                my: 'left center',
                at: 'right center',
                of: element,
                offset: '5 0'
            });

        },

		rules: {
			uname:{
				required:true,
				minlength:2,
				maxlength:20
			},
			email:{
				required:true,
				email:true,
				remote: "check-email.php"
			},
			confirmemail: {
				equalTo: "#email"
			},
			password:{
				required:true,
				minlength:'6'
			},
			/*confpassword:{
				required:true,
				equalTo:"#email"
			},*/
			terms:{
				required:true
			}
		},
		messages: {
			uname:{
				required: "Please enter your full name.",
				minlength:"Please enter at least 2 characters.",
				maxlength:"Please enter maximum 20 characters."
			},
			email:{
				required:"Please enter email address.",
				email:"Please enter valid email address.",
				remote:"Please choose a different email address."
			},
			confirmemail:{
				required:"Please confrim email address.",
				email:"Please enter your email same as above."
			},
			password:{
				required:  "Please enter a password.",
				minlength: "At least 6 characters."
			},
			/*confpassword:{
				required:"Please re-enter email address.",
				equalTo:"Please re-enter email address."
			},*/
			terms:{
				required: "Please check the box to accept the terms of use."
			}
		}

	});	
	
		jQuery("#editForm").validate({
		errorElement:'div',
		errorPlacement: function (error, element) {
            error.insertAfter(element).position({
                my: 'left center',
                at: 'right center',
                of: element,
                offset: '5 0'
            });

        },

		rules: {
			uname:{
				required:true,
				minlength:2,
				maxlength:20
				
			},
			email:{
				required:true,
				email:true,
				remote: "check-email.php"
			},
			password:{
				minlength:'6'
			}
		},
		messages: {
			uname:{
				required: "Please enter User name            ",
				minlength:"Please enter at least 2 characters",
				maxlength:"Please enter maximum 20 characters"
			},
			email:{
				required:"Please enter email address",
				email:"Please enter valid email address",
				remote:"This Email address already exist."
			},
			password:{
				minlength: "Password length At least 6 characters  "
			}
		}

	});
		// Reset Password
		jQuery("#resetpswdForm").validate({
		errorElement:'div',
		errorPlacement: function (error, element) {
            error.insertAfter(element).position({
                my: 'left center',
                at: 'right center',
                of: element,
                offset: '5 0'
            });

        },

		rules: {
			old_pswd:{
				required:true,
				remote: "check-password.php"
				
			},
			password:{
				required:true,
				minlength:'6'

			},
			conf_password:{
				required:true,
				equalTo:"#password"
			}
		},
		messages: {
			old_pswd:{
				required: "Please enter current password.",
				remote:"Please enter correct current password."
			},
			password:{
				required:"Please enter new password.",
				minlength: "Password length At least 6 characters."
			},
			conf_password:{
				required:"Please re-enter new password.",
				equalTo:"Please re-enter new password."
			}
		}

	});
	
	jQuery.validator.addMethod("ccode", function(value, element, param) {
  return this.optional(element) || value !== param;
}, "Please enter code to earn discount coupon.");
	
		jQuery("#codefrm").validate({
		errorElement:'div',
		
		rules: {
			Search:{
				required:true,
				/*remote: "check-coupon-code.php",*/
				ccode:'ACCEPTED'
			}
		},
		messages: {
			Search:{
				required:"Please enter a valid code."
				<!--remote:"This code you have already used. Please try another coupon."-->
			}
		}
	});	
	
	
	// user add form

	jQuery("#adduserFrmt").validate({
		errorElement:'div',

		rules: {
			uname:{
				required:true,
				minlength:2,
				maxlength:20,
				remote: SITEROOT+"/check-email.php"
			},
			FirstName:{
				required:true,
				minlength:2,
				maxlength:20
			},
			LastName:{
				required:true,
				minlength:2,
				maxlength:20
			},
			email:{
				required:true,
				email:true
			},
			password:{
				required:true,
				minlength:'5'
			},
			status:{
				required:true
			}
		},
		messages: {
			uname:{
				required: "Please enter full name.",
				minlength:"Please enter at least 2 characters",
				maxlength:"Please enter maximum 20 characters",
				remote:   "User name already exist.          "
			},
			FirstName:{
				required: "Please enter first name           ",
				minlength:"Please enter at least 2 characters",
				maxlength:"Please enter maximum 20 characters"
			},
			LastName:{
				required: "Please enter last name            ",
				minlength:"Please enter at least 2 characters",
				maxlength:"Please enter maximum 20 characters"
			},
			email:{
				required:"Please enter valid email address",
				email:   "Please enter valid email address"
			},
			password:{
				required:  "Please enter password",
				minlength: "At least 5 characters"
			},
			status:{
				required:"Please select status"
			}
		}

	});
	
	// user edit form
	function getUrlVars() {
    var vars = {};
    var parts = window.location.href.replace(/[?&]+([^=&]+)=([^&]*)/gi, function(m,key,value) {
        vars[key] = value;
    });
    return vars;
}
	var usrid = getUrlVars()["id"]; 
	
	async= "false";
	jQuery("#upduserFrmt").validate({
		errorElement:'div',

		rules: {
			uname:{
				required:true,
				minlength:2,
				maxlength:20,
				remote: SITEROOT+"/check-email.php?uid="+usrid
			},
			email:{
				required:true,
				email:true
			},
			phone:{
				required:true,
				integer: true
			},
			company:{
				required:true
			},
			address:{
				required:true
			}
		},
		messages: {
			uname:{
				required: "Please enter User name",
				minlength:"Please enter at least 2 characters",
				maxlength:"Please enter maximum 20 characters",
				remote:"User name already exist."
			},
			email:{
				required:"Please enter valid email address",
				email:"Please enter valid email address"
			},
			phone:{
				required:"Please enter phone number",
				integer:"Please enter only number"
			},
			company:{
				required:"Please enter company name"
			},
			address:{
				required:"Please enter address"
			}
		}

	});	
	
	
	//forgot password
	jQuery("#forgotpassForm").validate({
		errorElement:'div',
errorPlacement: function (error, element) {
            error.insertAfter(element).position({
                my: 'left center',
                at: 'right center',
                of: element,
                offset: '5 0'
            });
        },
		rules: {
			email:{
				required:true,
				email:true
			}
		},
		messages: {
			email:{
				required:"Please enter your email address.",
				email:"Please enter valid email address."
			}
		}
	});
	
	 jQuery("#adminloginForm").validate({
		errorElement:'div',

		rules: {
			username:{
				required:true,
				minlength:'2'
			},
			password:{
				required:true,
				minlength:'5'
			}
		},
		messages: {
			username:{
				required: "Please enter a username.",
				minlength: "At least 2 characters."
			},
			password:{
				required: "Please provide a password.",
				minlength: "At least 5 characters long."
			}
		},
 		success: function(div){
 			div.hide();
  		}
	});	
	
	
	jQuery("#frmcms").validate({
		errorElement:'div',
		rules: {
			name:{
			required:true,
			minlength:'2'
			},
		test:{
			required:true,
			minlength:'2'
			}
		},
	messages: {
			name:{
			required: "Please enter the name",
			minlength: "At least 2 characters"
			},
			test:{
			required: "Please enter the description",
			minlength: "At least 2 characters"
			}
		}
});
	
		//order form
	jQuery("#orderForm").validate({
		ignore: [],
		errorElement:'div',

		rules: {
			uname:{
				required:true
			},
			phone:{
				required:true,
				integer: true
			},
			email:{
				required:true,
				email: true
			},
			address:{
				required:true
			},
			streetName1:{
				required:true
			},
			city1:{
				required:true
			},
			postalCode1:{
				required:true
			},
			streetName:{
				required:true
			},
			city:{
				required:true
			},
			postalCode:{
				required:true
			},
			credit_card_no:{
				required:true,
				number: true,
				minlength: '16',
				maxlength: '16'
			},
			exp_date:{
				required:true
			},
			card_holder_name:{
				required:true
			},
			hidden_field:{
				required:true
			}
		},
		messages: {
			uname:{
				required:"Please enter user name"
			},
			phone:{
				required:"Please enter phone number",
				integer:"Please enter only number"
			},
			email:{
				required:"Please enter email address",
				email:"Please enter valid email address"
			},
			address:{
				required:"Please enter address"
			},
			streetName1:{
				required:"Please enter street name"
			},
			city1:{
				required:"Please enter city"
			},
			postalCode1:{
				required:"Please enter postal code"
			},
			streetName:{
				required:"Please enter street name"
			},
			city:{
				required:"Please enter city"
			},
			postalCode:{
				required:"Please enter postal code"
			},
			credit_card_no:{
				required:"Please enter credit card number",
				number: "Please enter only number",
				minlength: "Please enter 16 digits number",
				maxlength: "Please enter 16 digits number"
			},
			exp_date:{
				required:"Please enter expiry date of credit card"
			},
			card_holder_name:{
				required:"Please enter card holder name"
			},
			hidden_field:{
				required:" "
			}
		}
	});
	

	
	jQuery("#contactfrm").validate({
		errorElement:'div',

		rules: {
			name:{
				required:true,
				minlength:2,
				maxlength:100
			},
			email:{
				required:true,
				email:true
			},
			phone:{
				required:true,
				integer:true,
				minlength:10,
				maxlength:15
			},
			company:{
				required:true
			},
			message:{
				required:true
			},
			captchaname:{
				required:true,
				equalTo:"#check"
			}
		},
		messages: {
			name:{
				required: "Please enter User name",
				minlength:"Please enter at least 2 characters",
				maxlength:"Please enter maximum 100 characters"
			},
			email:{
				required:"Please enter email address",
				email:"Please enter valid email address"
			},
			phone:{
				required:"Please enter phone number",
				integer:"Please enter only number",
				minlength:"Please enter at least 10 characters",
				maxlength:"Please enter maximum 15 characters"
			},
			company:{
				required:"Please enter company name"
			},
			message:{
				required:"Please enter message"
			},
			captchaname:{
				required:"Please enter captcha value",
				equalTo:"Please enter correct captcha code"
			}
		}

	});	
	
	
/*jQuery("#msg").fadeOut(30000);*/
});