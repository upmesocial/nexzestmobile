<?php
include("Query.Inc.php");

$obj = new Query($DBName);
if ($obj->isValidSession()) {
	$obj->Redirect('dashboard.php');
}
if (isset($_SESSION['non_verfied'])) {
    $obj->Redirect('dashboard.php');
}

?>

<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<title>Welcome to Nexzest</title>
<link rel="stylesheet" href="css/style.css" />
</head>

<body>
<div id="wrapper">
    <header class="main_header anonymos">
    	<a href="index.php"><img src="img/logo.png" class="logo" style="border:none;" /></a>
        <div class="login_btn">
       		<a href="login.php"><img src="img/login.png" style="border:none;" /></a>
        </div>
        <div style="clear:both;"></div>
    </header>
        
    <section class="main_container">
    
    <div class="element_holder">
    	        <div class="welcome_text">
        	<a href="register.php"><img src="img/register.png" class="register_btn" /></a>
            <a href="learn_more.php"><img src="img/learn_more.png" class="learnmore_btn" /></a>
            <!--<p><strong>INTERACT<br /> WITH YOUR<br /> MAGAZINE</strong></p>-->
			<img src="img/w_bul.png" />
        </div>
        <div class="cover_image">
         <img src="img/nexzest_cover.png" />
        </div>
      <div style="clear:both;"></div>  
    </div>

    <div style="clear:both;"></div>
    </section>
    
    
    <div class="top_footer">
    	<h3>CONNECTING YOU WITH LOCAL BUSINESSES</h3>
    	<p>Call Us Now: 267-272-1326  |  EMAIL: <span class="yellow_font">INFO@NEXZEST.COM</span></p>	
    </div>
    <footer>
    	Copyright 2013 Nexzest. All Rights Reserved  |<br /> Call Us Now: 267-272-1326 
        
        <div class="social_icons">
        <a href="#"><img src="img/tw.png" /></a>
        <a href="#"><img src="img/yt.png" /></a>
        <a href="#"><img src="img/fb.png" /></a>
        <a href="#"><img src="img/in.png" /></a>
        <a href="#"><img src="img/p.png" /></a>
        <a href="#"><img src="img/c.png" /></a>
     </div>
<div class="patent_pending"> Patent Pending </div>
    <div style="clear:both;"></div>                       
    </footer>
</div>
</body>
</html>
<?php
	require_once "scripts.php";
?>