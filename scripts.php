<script type="text/javascript"> 
	var SITEROOT = "<?php echo $SiteUrl; ?>";
</script>
<script src="js/jquery.min.js"></script>
<script src="js/jquery-migrate.min.js"></script>
<script src="js/jquery-ui.min.js"></script>
<script src="js/jquery.ui.touch-punch.min.js"></script>
<script src="js/jquery.validate.js" type="application/javascript"></script>
<script src="js/common.js"></script>
<script type="text/javascript">
$(document).ready(function() {

var resize = function() {
	var windowHeight = $(window).height(),
	footerHeight = Math.round($('.top_footer').outerHeight() + $('footer').outerHeight()),
	containerTopOffset = $('section').offset().top,
	containerPadding = parseInt($('section').css('padding-top').replace('px')) + parseInt($('section').css('padding-bottom').replace('px')),	
	actualContainerMinHeight = windowHeight - (footerHeight + containerTopOffset + containerPadding),
	sectioHeight = actualContainerMinHeight;
	//sectioHeight = (actualContainerMinHeight > $('section').outerHeight()) ? actualContainerMinHeight : $('section').outerHeight();
	$('section').css('min-height', sectioHeight+'px');
	console.log(windowHeight+" == "+(footerHeight)+" == "+sectioHeight+" == "+containerPadding+" == "+containerTopOffset+" == "+($('section').outerHeight() - containerPadding));
}
$(window).bind("resize",function(){
	resize();
});

resize();

});

</script>
