<?php
include("Query.Inc.php");

$obj = new Query($DBName);
if ($obj->isValidSession()) {
	$obj->Redirect('dashboard.php');
}

?>
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<title>Loign - Nexzest</title>
<link rel="stylesheet" href="css/style.css" />
</head>

<body>
<div id="wrapper">
    <header class="main_header anonymos">
    	<a href="index.php"><img src="img/logo.png" class="logo"  style="border:none;" /></a>
        <div class="login_btn">
       		<a href="login.php"><img src="img/login.png" /></a>
        </div>
        <div style="clear:both;"></div>
    </header>
    <section class="login_main_container" style="background-size:100%; background-position:0 0;  background-repeat:repeat-y;">
        <div class="logout_action_container" style="max-width:none; width:80%; padding:20px;">
      <h2>Terms of Use</h2>
      <h3>Agreement between user and www.nexzest.com</h3>
      <p> Welcome to www.nexzest.com. The www.nexzest.com website (the "Site) is comprised of various web pages operated by Nexzest Inc ("Nexzest").  www.nexzest.com is offered to you conditioned on your acceptance without modification of the terms, conditions, and notices contained herein (the "Terms"). Your use of www.nexzest.com constitutes your agreement to all such Terms. Please read these terms carefully, and keep a copy of them for your reference. </p>
      <p> www.nexzest.com is a Social Networking Site </p>
      
      <p>The consumer is lead to a specific social media page. www.nexzest.com rewards the user if he or sheconducts a directed interaction with the specific social media.</p>
      
      <h4>Privacy</h4>
      <p>Your use of www.nexzest.comis subject to Nexzest's Privacy Policy. Please review our Privacy Policy, which also governs the Site and informs users of our data collection practices.</p>
      
      <h4>Electronic Communications</h4>
		<p>Visitingwww.nexzest.com or sending emails to Nexzest constitutes electronic communications. You consent to receive electronic communications and you agree that all agreements, notices, disclosures and other communications that we provide to you electronically, via email and on the Site, satisfy any legal requirement that such communications be in writing.</p>      
        
        <h4>Your account </h4>
        <p>
        
        If you use this site, you are responsible for maintaining the confidentiality of your account and password and for restricting access to your computer, and you agree to accept responsibility for all activities that occur under your account or password. You may not assign or otherwise transfer your account to any other person or entity. You acknowledge that Nexzest is not responsible for third party access to your account that results from theft or misappropriation of your account.  Nexzest and its associates reserve the right to refuse or cancel service, terminate accounts, or remove or edit content in our sole discretion.
        </p>
      <p>
      Nexzest does not knowingly collect, either online or offline, personal information from persons under the age of thirteen. If you are under 18, you may use www.nexzest.com only with permission of a parent or guardian.
      </p>
      
      <h4>Links to third party sites/Third party services</h4>
      <p>www.nexzest.com may contain links to other websites ("Linked Sites"). The Linked Sites are not under the control of Nexzest and Nexzest is not responsible for the contents of any Linked Site, including without limitation any link contained in a Linked Site, or any changes or updates to a Linked Site. Nexzest is providing these links to you only as a convenience, and the inclusion of any link does not imply endorsement by Nexzest of the site or any association with its operators.</p>
      
      <p>Certain services made available via www.nexzest.com are delivered by third party sites and organizations. By using any product, service or functionality originating from the www.nexzest.comdomain, you hereby acknowledge and consent that Nexzest may share such information and data with any third party with whom Nexzest has a contractual relationship to provide the requested product, service or functionality on behalf of www.nexzest.com users and customers.</p>
      
      <h4>No unlawful or prohibited use/Intellectual Property</h4>
      <p>
      You are granted a non-exclusive, non-transferable, revocable license to access and use www.nexzest.com strictly in accordance with these terms of use. As a condition of your use of the Site, you warrant to Nexzest that you will not use the Site for any purpose that is unlawful or prohibited by these Terms. You may not use the Site in any manner which could damage, disable, overburden, or impair the Site or interfere with any other party's use and enjoyment of the Site.  You may not obtain or attempt to obtain any material or information through any means not intentionally made available or provided for through the Site.
      </p>
      <p>
      All content included as part of the Service, such as text, graphics, logos, images, as well as the compilation thereof, and any software used on the Site, is property of Nexzest or its suppliers and protected by copyright and other laws that protect intellectual property and proprietary rights. You agree to observe and abide by all copyright and other proprietary notices, legends or other restrictions contained in any such content and will not make any changes thereto.
      </p>
      <p>
      You will not modify, publish, transmit, reverse engineer, participate in the transfer or scale, create derivative works, or in any way exploit any of the content, in whole or in part, found on the Site. Nexzest content is not for resale. Your use of the Site does not entitle you to make any unauthorized use of any protected content, and in particular you will not delete or altar any proprietary rights or attribution notices in any content. You will use protected content solely for your personal use, and will make no other use of the content without the express written permission of Nexzest and the copyright owner. You agree that you do not acquire any ownership rights in any protected content. We do not grant you any licenses, express or implied, to the intellectual property of Nexzest or our licensors except as expressly authorized by these Terms.
      </p>
      <h4>Use of communication services</h4>
      <p>The Site may contain bulletin board servies, chat areas, news groups, forums, communities, personal web pages, calendars, and/or other message or communication facilities designed to enable you to communicate with the public at large or with a group (collectively, "Communication Services"), you agree to use the Communication Services only to post, send  and receive messages and material that are proper and related to the particular Communication Service.</p>
      <p>By way of example, and not as a limitation, you agree that when using a Communication Service, you will not: defame, abuse, harass, stalk, threaten or otherwise violate legal rights (such as rights of privacy and publicity) of others;  publish, post, upload, distribute or disseminate any inappropriate, profane, defamatory, infringing, obscene, indecent or unlawful topic, name, material or information; upload files that contain software or other material protected by intellectual property laws (or by rights of privacy or publicity) unless you own or control the rights thereto or  have received all necessary consents; upload files that contain viruses, corrupted files, or any other similar software or programs that may damage the operation of another's computer; advertise or offer to sell or buy any goods or services for any business purpose, unless such Communication Service specifically allows such messages; conduct or forward surveys, contests, pyramid schemes or chain letters; download any file posted by another user of a Communication Service that you know, or reasonably should know, cannot be  legally distributed in such manner; falsify or delete any author attributions, legal or other proper notices or proprietary designations or labels of the origin or source of software or other material contained in a file that is uploaded, restrict or inhibit any other user from using and enjoying the Communication Services; violate any code or conduct or other guidelines which may be applicable for any particular Communication Service; harvest or otherwise collect information about others, including e-mail addresses, without their consent; violate any applicable laws or regulations.</p>
      <p>
      Nexzest has no obligation to monitor the Communication Services. However, Nexzest reserves the right to review materials posted to a Communication Service and to remove any materials in its sole discretion. Nexzest reserves the right to terminate your access to any or all of the Communication Services at any time without notice for any reason whatsoever.
      </p>
      <p>
      Always use caution when giving out any personally identifying information about yourself or your children in any Communication Service. Nexzest does not control or endorse the content, messages or information found in any Communication Service and, therefore, Nexzest specifically disclaims any liability with regard to the Communication Services and any actions resulting from your participating in any Communication Service. Managers and hosts are not authorized Nexzest  spokespersons, and their views do not necessarily reflect those of Nexzest.
      </p>
      <p>
      Materials uploaded to a Communication Service may be subject to posted limitations on usage, reproduction and/or dissemination. You are responsible for adhering to such limitations if you upload the materials.
      </p>
      
      <h4>Materials provided to www.nexzest.com or posted on any Nexzest web page</h4>
      <p>Nexzest does not claim ownership of the materials you provide to www.nexzest.com (including feedback and suggestions) or post, upload, input or submit to any Nexzest Site or our associated services (collectively "Submissions"). However, by posting, uploading, inputting, providing or submitting your Submission you are granting Nexzest, our affiliated companies and necessary sublicensees permission to use your Submission in connection  with our operation of their Internet businesses, including, without limitation, the rights to: copy, distribute, transmit, publicly display, publicly perform, reproduce, edit, translate and reformat your Submission; and to publish your name in connection with your Submission.</p>
		
        
        <p>
        No compensation will be paid with respect to the use of your Submission, as provided herein. Nexzest is under no obligation to post or use any Submission you may provide and may remove any Submission at any time in Nexzest's sole discretion.
        </p>      
      <p>
      By posting, uploading, inputting, providing or submitting your Submission you warrant and represent that you own or otherwise control all of the rights to your Submission as described in this section including, without limitation, all the rights necessary for you to provide, post, upload, input or submit the Submissions.
      </p>
      
      <h4>Third Party Accounts</h4>
      
      <p>
      You will be able to connect your Nexzest account to third party accounts. By connecting your Nexzest account to your third party account, you acknowledge and agree that you are consenting to the continuous release of information about you to others (in accordance with your privacy settings on those third party sites). If you do not want information about you to be shared in this manner, do not use this feature.
      </p>
      
      <h4>International Users</h4>
      <p>The Service is controlled, operated and administered by Nexzest from our offices within the USA. If you access the Service from a location outside the USA, you are responsible for compliance with all local laws. You agree that you will not use the Nexzest Content accessed through www.nexzest.com in any country or in any manner prohibited by any applicable laws, restrictions or regulations.</p>
      
      <h4>Indemnification</h4>
      <p>You agree to indemnify, defend and hold harmless Nexzest, its officers, directors, employees, agents and third parties, for any losses, costs, liabilities and expenses (including reasonable attorneys' fees) relating to or arising out of your use of or inability to use the Site or services, any user postings made by you, your violation of any terms of this Agreement or your violation of any rights of a third party, or your violation of any applicable laws, rules or regulations. Nexzest reserves the right, at its own cost, to assume the exclusive defense and control of any matter otherwise subject to indemnification by you, in which event you will fully cooperate with Nexzest in asserting any available defenses.</p>
      
      <h4>Liability disclaimer</h4>
      <p>
      The information, software products, and services included in or available through the site may include inaccuracies or typographical errors. Changes are periodically added to the information herein. Nexzest Inc  and/or its suppliers may make improvements and/or changes in the site at any time.
      </p>
      
      <p>
      Nexzest Inc  and/or its suppliers make no representations about the suitability, reliability, availability, timeliness, and accuracy of the information, software, products, services and related graphics contained on the site for any purpose. To the maximum extent permitted by applicable law, all such information, software, products, services and related graphics are provided "as is" without warranty or condition of any kind. Nexzest Inc  and/or its suppliers hereby disclaim all warranties and conditions with regard to this information, software, products, services and related graphics, including all implied warranties or conditions of merchantability, fitness for a particular purpose, title and non-infringement.
      </p>
      
      <p>
      To the maximum extent permitted by applicable law, in no event shall Nexzest Inc and/or its suppliers be liable for any direct, indirect, punitive, incidental, special, consequential damages or any damages whatsoever including, without limitation, damages for loss of use, data or profits, arising out of in any way connected with the use or performance of the site, with the delay or inability to use the site or related services, the provision of or products, services and related graphics obtained through the site, or otherwise arising out of the use of the site, whether based on the contract, tort, negligence, strict liability or otherwise, even if Nexzest Inc or any of its suppliers has been advised of the possibility of damages. Because some states/jurisdictions do not allow the exclusion or limitation of liability for consequential or accidental damages, the above limitation may not apply to you.  If you are dissatisfied with any portion of the site, or any of these terms of use, your sole and exclusive remedy is to discontinue using the site.
      </p>
      
      <h4>Termination/access restriction</h4>
      
      <p>
      Nexzest reserves the right, in is sole discretion, to terminate your access to the Site and the related services or any portion thereof at any time, without notice. To the maximum extent permitted by law, this agreement is governed by the laws of the State of Pennsylvania and you hereby consent to the exclusive jurisdiction and venue of courts in Pennsylvania in all disputes arising out of or relating to the use of the Site. Use of the Site is unauthorized in any jurisdiction that does not give effect to all provisions of these Terms, including, without limitation, this section.
      </p>
      <p>
      You agree that no joint venture, partnership, employment, or agency relationship exists between you and Nexzest as a result of this agreement or use of the Site. Nexzest's performance of this agreement is subject to existing laws and legal process, and nothing contained in this agreement is in derogation of Nexzest's right to comply with governmental, court and law enforcement requests or requirements relating to your use of the Site or information provided to or gathered by Nexzest with respect to such use. If any part of this agreement is determined to be invalid or unenforceable pursuant to applicable law including, but not limited to, the warranty disclaimers and liability limitations set forth above, then the invalid or unenforceable provision will be deemed superseded by a valid, enforceable provision that most closely matches the intent of the original provision and the remainder of the agreement shall continue in effect.
      </p>
      <p>
      Unless otherwise specified herein, this agreement constitutes the entire agreement between the user and Nexzest with respect to the Site and it supersedes all prior or contemporaneous communications and proposals, whether electronic, oral or written, between the user and Nexzest with respect to the Site.  A printed version of this agreement and of any notice given in electronic shall be admissible in judicial or administrative proceedings based upon or relating to this agreement to the same extent and subject to the same conditions as other business documents and records originally generated and maintained in printed form. It is the express wish to the parties that this agreement and all related documents be written in English.
      </p>
      
		<h4>Changes to Terms</h4>
       <p> Nexzest reserves the right, in its sole discretion, to change the Terms under which www.nexzest.com is offered. The most current version of the Terms will supersede all previous versions. Nexzest encourages you to periodically review the Terms to stay informed of our updates.
       </p>
       
       <h4>Contact Us</h4>
       <p>Nexzest welcomes your questions or comments regarding the Terms:</p>
       
       <h4>Nexzest Inc</h4>
       <p>Quakertown, Pennsylvaina 18951</p>
       
       <h4>Email Address:</h4>
       <p>nexzest@gmail.com</p>
       
       <h4>Telephone</h4>
       <p>267-272-1326</p>
       
       <p>&nbsp;</p>
       <p>Effective as of January 24, 2014</p>
       </div>
    </section>
    <div class="top_footer">
    	<h3>CONNECTING YOU WITH LOCAL BUSINESSES</h3>
    	<p>Call Us Now: 267-272-1326  |  EMAIL: <span class="yellow_font"><a href="mailto:info@nexzest.com">INFO@NEXZEST.COM</a></span></p>	
    </div>
    <footer>
    	Copyright 2013 Nexzest. All Rights Reserved  | <br /> Call Us Now: 267-272-1326 
        <div class="social_icons">
        <a href="#"><img src="img/tw.png" /></a>
        <a href="#"><img src="img/yt.png" /></a>
        <a href="#"><img src="img/fb.png" /></a>
        <a href="#"><img src="img/in.png" /></a>
        <a href="#"><img src="img/p.png" /></a>
        <a href="#"><img src="img/c.png" /></a>
         </div>
     <div class="patent_pending"> Patent Pending </div>
    <div style="clear:both;"></div> 
    </footer>
</div>
</body>
</html>
