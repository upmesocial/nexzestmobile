<?php 
include("Query.Inc.php") ;
$Obj = new Query($DBName) ;

$Id = $_SESSION['UserID'];

$ThisPageTable = 'users';
$DataArray = $Obj->Select_Dynamic_Query($ThisPageTable,'',array('id'),array('='),array($Id)) ;
$name = $DataArray[0]['name'] ;
$email = $DataArray[0]['email'] ;
$images = $DataArray[0]['images'] ;
$fb_profile_pic_url = $DataArray[0]['fb_profile_pic_url'] ;

?>
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<title>Profile - Nexzest</title>
<link rel="stylesheet" href="css/style.css" />
</head>

<body>
<div id="wrapper">
    <?php require_once "header.php"; ?>
    <section class="dashboard_main_container">
    	
        
     <div class="profile-container">
                <div class="profile-content">
                    <div class="profile-content-top">
                    <div class="profile_page_pic_display">
                    <?php 
						if($images != '')
						{ ?>
						<img width="70" height="70" src="<?php echo $CouponSiteUrl; ?>/uploads/user/<?php echo $images; ?>" />
						<?php 
						}
						else
						{
							if($fb_profile_pic_url != '')
							{
							?>
							<img alt="facebook profile" src="<?php echo $fb_profile_pic_url; ?>" width="70" height="70" />
						<?php
							}
							else
							{?>
							<img src="img/nexzestlogo.jpg" />
							<?php 
							} 						
						}
						?>
    </div>
                    <div class="profile_name">
					 <?php if (isset($_SESSION['UserID'])) {
					 	echo $name;
					 } else {
					 	echo $_SESSION['uname'];
					 } ?>
    				</div>

                    <?php if (isset($_SESSION['UserID'])) { ?>
					 	<div class="edit_profile_btn">
                   			<a class="edit" href="editprofile.php">Edit Profile</a>
                    	</div>
					 <?php } ?>
                    <div style="clear:both;"></div>
                    </div><!--left_content_top-->
                
                    <div class="profile-content-bottom">
                    <img src="img/NexzestConnections.jpg" />
                    
                    <header>
                    PAGES
                    </header>
                    <?php 
						$query_like_pages = "SELECT * from likedpages where user_id = '".$_SESSION['UserID']."' order by id DESC";
						$result_like_pages = mysql_query($query_like_pages);
						$counter=0;
						
						if (0 < count($result_like_pages)) {
							?>
                            <ul>
                            <?php
							while($row_like_page = mysql_fetch_array($result_like_pages))
							{
								$business_url = explode("/", $row_like_page['business_url']);
								if($business_url[3] == 'pages'){
									$business_page_name = $business_url[4];
								}else
								{
									$business_page_name = $business_url[3];
								}
							?>
							<li><a href="<?php echo $row_like_page['business_url'];?>" target="_blank" style="color:#F57711"><?php echo $business_page_name;?></a></li>
							<?php 
							}
							?>
                            </ul>
                            <?php
						}
							?>                    
                    
                    </div><!--left_content_bottom-->
                </div>
            </div><!--left_container-->   
        
       <div class="back"> <a href="index.php"><img src="img/back.png" /></a> </div>
     
    <div style="clear:both;"></div>
    </section>    
    <?php require_once "footer.php"; ?>    
</div>
</body>
</html>
<?php
	require_once "scripts.php";
?>