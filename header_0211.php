<?php
require_once("Query.Inc.php") ;
$obj = new Query($DBName);
if (!$obj->isValidSession()) {
	$obj->Redirect('index.php');
}

?>

<header class="main_header">
    	<a href="dashboard.php"><img src="img/n-logo.png" class="nlogo" /></a>      
        
        <a href="logout_action.php"><img src="img/sett_img.png" class="settings_btn" /></a>
        
        <div style="position:relative"><a href="coupon_holder.php"><img src="img/save_img.jpg" class="offer_btn" /></a>
        <?php 
					if(isset($_SESSION['congrats_msg']) && $_SESSION['congrats_msg'] == 'congrats')
					{?>
						<div id="congrats_coupon_earned" style="position:absolute; right:36px; top:40px;">
							<img src="img/received.png" />
						</div>
					<?php 
						unset($_SESSION['congrats_msg']);
					}?>
        </div>
        
         <a href="dashboard.php"><img src="img/code_img.png" class="code_btn" /></a>
        <div style="clear:both;"></div>
    </header>