<?php
include("Query.Inc.php");

$obj = new Query($DBName);
if ($obj->isValidSession()) {
	$obj->Redirect('dashboard.php');
}

?>
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<title>Loign - Nexzest</title>
<link rel="stylesheet" href="css/style.css" />
</head>

<body>
<div id="wrapper">
    <header class="main_header anonymos">
    	<a href="index.php"><img src="img/logo.png" class="logo"  style="border:none;" /></a>
        <div class="login_btn">
       		<a href="login.php"><img src="img/login.png" /></a>
        </div>
        <div style="clear:both;"></div>
    </header>
    <section class="registration_main_container">
        <div class="registration_container">
        <h1>SIGN UP NOW! <br /><span class="now">New on Nexzest?</span></h1>	
         <form id="singupForm" name="singupForm"  method="post" action="register_action.php">    
            <div class="field-component">
            	<input name="uname" id="uname" type="text" class="text_field input-field" placeholder="Full Name"/></div><br />
            
            <div class="field-component"><input name="email" id="email" type="email" class="text_field" placeholder="Email Address"/></div><br />
            
            <div class="field-component"><input name="confirmemail" id="confirmemail" type="email" class="text_field" placeholder="Re-enter Email Address"/></div><br />
          
        	<div class="field-component"><input name="password" id="password" type="password" class="text_field" placeholder="Password"/></div>
              <br />
               <div class="field-component"><input  style="margin:3px 0 0 0; float:left" type="checkbox" name="terms" value=""></div>
                            &nbsp;&nbsp;By clicking the button, you agree to our
                            <a href="terms_of_use.php"> 
                            <span style="color:#d34e01; text-transform:uppercase; letter-spacing:-0.5px; text-decoration:none;">terms of use</span>
                            </a>
                            <div for="terms" class="error" style="display:none; padding: 8px 3px 3px 20px;">Please select the term and condition</div>                   
           <input class="registration_sub" type="submit" name="register" value="Sign Up Now" >
          </form>
        </div>
        
        <div class="back"> <a href="index.php"><img src="img/back.png" /></a> </div>
    <div style="clear:both;"></div>
    </section>
    <div class="top_footer">
    	<h3>CONNECTING YOU WITH LOCAL BUSINESSES</h3>
    	<p>Call Us Now: 267-272-1326  |  EMAIL: <span class="yellow_font"><a href="mailto:info@nexzest.com">INFO@NEXZEST.COM</a></span></p>	
    </div>
    <footer>
    	Copyright 2013 Nexzest. All Rights Reserved  | <br /> Call Us Now: 267-272-1326 
        <div class="social_icons">
        <a href="#"><img src="img/tw.png" /></a>
        <a href="#"><img src="img/yt.png" /></a>
        <a href="#"><img src="img/fb.png" /></a>
        <a href="#"><img src="img/in.png" /></a>
        <a href="#"><img src="img/p.png" /></a>
        <a href="#"><img src="img/c.png" /></a>
         </div>
     <div class="patent_pending"> Patent Pending </div>
    <div style="clear:both;"></div> 
    </footer>
</div>
</body>
</html>
<?php
	require_once "scripts.php";
?>