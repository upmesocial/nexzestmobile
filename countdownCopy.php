<?php
include("Query.Inc.php") ;
$Obj = new Query($DBName) ;
?>
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<title>History of Local Winners - Nexzest</title>
<link rel="stylesheet" href="css/style.css" />
</head>

<body>
<?php
	require_once "scripts.php";
?>

<div id="wrapper">
    <?php require_once "header.php"; ?>
    <section class="dashboard_main_container">
    	
        
    <div class="countdown_container">
	<div class="countdown_box">
    	<div class="countdown_subtitle">NEXZEST CA<span style="color:#ff6300;">$</span>H</div>
        <div class="countdown_title">COUNTDOWN</div>
        
        	<div class="countdown_timer">
				<?php include_once('countdowntimer.php');?>
	        </div>
        
        <div class="local_winners">
        HISTORY OF LOCAL WINNERS
        </div>
        <?php
                $counterdates2 = date('m/d/Y g:ia');
                
                
                $sql3="select z.*,u.* 
                from zesthistory z 
                inner join users u 
                on z.uid=u.id 
                where z.addcountimes <= '".$counterdates2."'  
                order by z.windate DESC ";
                
                $DataArray22 = $Obj->select($sql3);
                
                if(!empty($DataArray22))
                {
                $CountDataArray22 = count($DataArray22); 
                }
                else
                {
                $CountDataArray22 = 0; 
                }
				
                if($CountDataArray22 >=1)
                {
                for($i=0;$i<$CountDataArray22;$i++) 
                {
                $queryComWin = "SELECT * FROM companies WHERE id = ".$DataArray22[$i]['companyid']." LIMIT 1";
                $resultComWin = mysql_query($queryComWin);
                $dataArrayComWin= mysql_fetch_array($resultComWin);
                $company_name = stripslashes($dataArrayComWin['name']);
                
                $addcountimes = $DataArray22[$i]['addcountimes'];
                ?>
        <div class="winner_info">
            <div class="profile_pic">
            	<?php 
                if($DataArray22[$i]['images'] != '')
                { ?>
                <img width="70" height="70" src="<?php echo $CouponSiteUrl; ?>/uploads/user/<?php echo $DataArray22[$i]['images']; ?>" />
                <?php 
                }
                else
                {
                if($DataArray22[$i]['fb_profile_pic_url'] != '')
                {
                ?>
                <img alt="facebook profile" src="<?php echo $DataArray22[$i]['fb_profile_pic_url']; ?>" width="70" height="70" />
                <?php
                }
                else
                {?>
                <img src="images/default_profile.png" />
                <?php 
                } 						
                }
                ?>
            </div>
            <div class="winner_detail">
            <div class="winner_name"><?php echo $DataArray22[$i]['name']; ?></div>
            <div class="cash_details">$<?php echo $DataArray22[$i]['cash']; ?> nexzest cash to</div>
            <div class="winner_company_name"><?php echo $company_name; ?></div>
            <div class="exp_date"><?php echo $addcountimes;?></div> 
            </div>
        </div>
        
        <?php      
                }
                }
				else
				{?>
					<h2>No Record Found</h2>
				<?php } 
                ?>
	</div>
</div>  
        
      
     
    <div style="clear:both;"></div>
    </section>    
    <?php require_once "footer.php"; ?>    
</div>
</body>
</html>