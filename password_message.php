<?php
include("Query.Inc.php") ;
include("Functions.Inc.php") ;
?>
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<title>Password changed - Nexzest</title>
<link rel="stylesheet" href="css/style.css" />
</head>

<body>
<div id="wrapper">
    <?php require_once "header.php";  ?>
    <section class="login_main_container">
        <div class="logout_action_container">
        	
           <div>Your password has been successfully changed.</div>
          
        </div>
        
        <div class="back"><a href="index.php"><img src="img/back.png" /></a> </div>
    <div style="clear:both;"></div>
    </section>
   <?php require_once "footer.php"; ?>
</div>
</body>
</html>
<?php
	require_once "scripts.php";
?>