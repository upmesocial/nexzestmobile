<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<title>Learn More</title>
<link rel="stylesheet" href="css/style.css" />
</head>

<body>
<div id="wrapper">
    <header class="main_header anonymos">
    	<a href="index.php"><img src="img/logo.png" class="logo" style="border:none;" /></a>
        <div class="login_btn">
       		<a href="login.php"><img src="img/login.png" style="border:none;" /></a>
        </div>
        <div style="clear:both;"></div>
    </header>
    <section class="main_container">
        <div class="element_holder">
            <iframe class="video_frame" width="320" height="345" src="http://www.youtube.com/embed/uN5aJaSC_sk" frameborder="0" ></iframe>
            <div style="clear:both;"></div>
        </div>
    <div class="back"> <a href="index.php"><img src="img/back.png" /></a> </div>
    </section>
    
    <div class="top_footer">
    	<h3>CONNECTING YOU WITH LOCAL BUSINESSES</h3>
    	<p>Call Us Now: 267-272-1326  |  EMAIL: <span class="yellow_font">INFO@NEXZEST.COM</span></p>	
    </div>
    <footer>
    	Copyright 2013 Nexzest. All Rights Reserved  |<br /> Call Us Now: 267-272-1326 
        
        <div class="social_icons">
        <a href="#"><img src="img/tw.png" /></a>
        <a href="#"><img src="img/yt.png" /></a>
        <a href="#"><img src="img/fb.png" /></a>
        <a href="#"><img src="img/in.png" /></a>
        <a href="#"><img src="img/p.png" /></a>
        <a href="#"><img src="img/c.png" /></a>
     </div>
<div class="patent_pending"> Patent Pending </div>
    <div style="clear:both;"></div>                       
    </footer>
</div>
</body>
</html>
<?php
	require_once "scripts.php";
?>