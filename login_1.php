<?php
include("Query.Inc.php");

$obj = new Query($DBName);
if ($obj->isValidSession()) {
	$obj->Redirect('dashboard.php');
}

?>
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<title>Loign - Nexzest</title>
<link rel="stylesheet" href="css/style.css" />
<script type="application/javascript" src="http://connect.facebook.net/en_US/all.js"></script>
<script>
window.fbAsyncInit = function() {
	FB.init({
	   appId: '348269895312107',
	   cookie: true,
	   xfbml: true,
	   oauth: true
	}); 
};

</script>
</head>

<body>
<div id="wrapper">
    <header class="main_header anonymos">
    	<a href="index.php"><img src="img/logo.png" class="logo"  style="border:none;" /></a>
        <div class="login_btn">
       		<a href="login.php"><img src="img/login.png" style="border:none;" /></a>
        </div>
        <div style="clear:both;"></div>
    </header>
    <section class="login_main_container">
        <div class="login_container">
        <h1>LOGIN</h1>	
         <form name="loginForm" id="loginForm" action="loginCheck.php" method="post">    
          <p> Email: </p>
            <input name="username" id="username" type="text" class="text_field"/><br />
          <p> Password: </p>
        	<input class="text_field" name="password" type="password"/>
              <br />                     
           <input class="login_sub" type="button" onClick="confimrLogin()" name="LOGIN" value="LOGIN" >
            
            <a class="forget" href="forgot-password.php">Forgot password?</a>
          </form>
          <div style="clear:both;"></div>
        </div>
        
        <div class="back"><a href="index.php"><img src="img/back.png" /></a> </div>
    <div style="clear:both;"></div>
    </section>
    <div class="top_footer">
    	<h3>CONNECTING YOU WITH LOCAL BUSINESSES</h3>
    	<p>Call Us Now: 267-272-1326  |  EMAIL: <span class="yellow_font"><a href="mailto:info@nexzest.com">INFO@NEXZEST.COM</a></span></p>	
    </div>
    <footer>
    	Copyright 2013 Nexzest. All Rights Reserved  | <br /> Call Us Now: 267-272-1326 
        <div class="social_icons">
        <a href="#"><img src="img/tw.png" /></a>
        <a href="#"><img src="img/yt.png" /></a>
        <a href="#"><img src="img/fb.png" /></a>
        <a href="#"><img src="img/in.png" /></a>
        <a href="#"><img src="img/p.png" /></a>
        <a href="#"><img src="img/c.png" /></a>
         </div>
     <div class="patent_pending"> Patent Pending </div>
    <div style="clear:both;"></div> 
    </footer>
</div>
</body>
</html>
<?php
	require_once "scripts.php";
?>

<script>

function getFacebookUser(callback){
	FB.getLoginStatus(function(response) {
	if (response.status === 'connected') {
		// the user is logged in and has authenticated your app
		var uid = response.authResponse.userID;
		var accessToken = response.authResponse.accessToken;
		callback(response);
				
	  } else if (response.status === 'not_authorized') {
		// the user is logged in to Facebook, 
		// but has not authenticated your app
		callback(null);
	  } else {
		// the user isn't logged in to Facebook.
		callback(null);
	  }
	});
}

loginToNexZest = function() {
	$('#loginForm').submit();	
}

confimrLogin = function() {
	loginToNexZest();	
	$.ajax({
		url:"getFbPass.php",
		data:{username:$('#username').val()},
		type:"GET",
		contentType:"text/html",
		success: function(response){
			console.log(response);
			if("" != response && null != response) {
			
				var obj = eval("(" + response + ')');
				console.log(obj.user_id);	
				
				getFacebookUser(function(response){
					if (null !== response) {
						console.log(response.authResponse.userID);
						if (obj.user_id == response.authResponse.userID) {
							loginToNexZest();
						}
						else {
							if(confirm('You seems to have logged into facebook using different facebook account. Are sure you wanna continue ?')){
								FB.logout(function(response) {
								  // user is now logged out
								  loginToNexZest();
								});		
								
							}
						}
					}
					else {
						loginToNexZest();	
					}
				});

			}
			else loginToNexZest();
		}, error: function(error){
			alert('Error Occured! Please try later.');
		}
	});
	
	
	/*getFacebookUser(function(response){
		if (null !== response) {
			console.log(response.authResponse.userID);
			if (obj.user_id == response.authResponse.userID) {
				loginToNexZest();
			}
			else {
				if(confirm('You seems to have logged into facebook using different facebook account. Are sure you wanna continue ?')){
					FB.logout(function(response) {
					  // user is now logged out
					  loginToNexZest();
					});		
					
				}
			}
		}
		else {
			loginToNexZest();	
		}
	});*/
}

</script>
