<?php 
include("Query.Inc.php") ;
include("Functions.Inc.php") ;

$Obj = new Query($DBName) ;
$Msg="";
$ThisPageTable = 'users';

if(isset($_POST['register']))
{ 
	$uname = mysql_real_escape_string(ucfirst(trim($_POST['uname'])));
	$email = mysql_real_escape_string(trim($_POST['email']));
	$_SESSION['email'] = $email;
	$_SESSION['user_name'] = $uname;
	$_SESSION['non_verfied'] = 'yes';
	$password = mysql_real_escape_string($_POST['password']);
	$add_date = date('y-m-d H:i:s');	
	$ValueArray = array($uname,$email,$password,$add_date);
	$FieldArray = array('name','email','password','add_date');
	$Success = $Obj->Insert_Dynamic_Query($ThisPageTable,$ValueArray,$FieldArray);
	
	$newid = base64_encode(mysql_insert_id());

	$fields = array('activation_code');
	$activation_link = "http://nexzest.com/mobile/verify.php?val=".$newid."" ;
	$values = array($activation_link);
	$succ = $Obj->Update_Dynamic_Query2($ThisPageTable,$values,$fields,$_SESSION['email']);

	$to = $email; 
	$subject1= $SITENAME." Registration Confirmation"; 
	$headers1 = 'MIME-Version: 1.0' . "\r\n";
	$headers1.= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
	$headers1.="From: ".$SITENAME." <donot-reply@nexzest.com>";
	$message1 = "";
	$message1="You have been successfully registered at ".$SITENAME.". <br/>";
	$message1.="Click on this link to login to your account <br/>";
	$message1.="<a href='".SITEROOT."/verify.php?val=".$newid."'>Click Here To Verify</a>" ;
	$message = file_get_contents('mails.html');
	$message = str_replace('[[sitename]]', $SITENAME, $message);
	$message = str_replace('[[siteroot]]', SITEROOT, $message);
	$message = str_replace('[[msgdata]]', $message1, $message); 
	
	$file= mail($to,$subject1,$message,$headers1);
	
	
	if($Success > 0)
	{
		$Msg = "Your account has been made. Please verify it by clicking the activation link that has been send to your email.";  
		$_SESSION['msg'] =$Msg;
		
		/* start -  sent to admin email So that we are notified when  a new user is created, (id, user name, email, join date)   */
		
		$user_id = mysql_insert_id();
		$join_date = date('m/d/y');
		
		$message='';
		$message = '<html><body>';	
		$message .= '<table cellpadding="10" width="100%">';	
		$message .= "<tr><td >Hello Admin,</td></tr>";
		$message .= "<tr><td >New user has been created account with below details.</td></tr>";			
		$message .= "<tr><td><strong>ID:</strong> </td><td>" . $user_id. "</td></tr>";	
		$message .= "<tr><td><strong>USERNAME:</strong> </td><td>" . $uname . "</td></tr>";	
		$message .= "<tr><td><strong>EMAIL:</strong> </td><td>" . $email . "</td></tr>";	
		$message .= "<tr><td><strong>JOIN DATE:</strong> </td><td>" . $join_date . "</td></tr>";
		$message .= "<tr><td >Thanks,</td></tr>";
		$message .= "<tr><td >Nexzest</td></tr>";		
		
		$message .= "</table>";
		$message .= "</body></html>";	
		
		$to = '';
		$to  = 'nexzest@gmail.com'; 
		
		$from = '';
		$from  = 'donot-reply@nexzest.com';
		
		$subject='';
		$subject = 'NEXZEST Notification - New user is created ';
		
		$headers='';
		$headers = "From: " . $from . "\r\n";
		$headers .= "Reply-To: ". $from . "\r\n";
		$headers .= "MIME-Version: 1.0\r\n";
		$headers .= "Content-Type: text/html; charset=ISO-8859-1\r\n";
		mail($to, $subject, $message, $headers);	
			
	}
	else
	{
		$Msg = "<div id='unsucc'>$R_N_I_S</div>";  
		$_SESSION['msg'] =$Msg;
	}
	$Obj->Redirect('dashboard.php');exit;
}
?>