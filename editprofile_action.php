<?php 
include("Query.Inc.php") ;
include("Functions.Inc.php") ;

$Obj = new Query($DBName) ;

$Msg="";
$ThisPageTable = 'users';
$Id = $_SESSION['UserID'];

		
if(isset($_POST['uname']))
{
	$uname = mysql_real_escape_string(ucfirst(trim($_POST['uname'])));
	$email = mysql_real_escape_string(trim($_POST['email']));
	$urlval= $_POST['cururl'];
	
	if(isset($_POST['oldurl']) && ($_POST['oldurl'] != ''))
	{
		$oldurl= $_POST['oldurl'];
		$img_name = $oldurl ;
	}
	
	if(@$_FILES['image']['name'] != "")
	{   
		if ((($_FILES["image"]["type"] == "image/gif") || ($_FILES["image"]["type"] == "image/jpeg") || ($_FILES["image"]["type"] == "image/jpg")|| ($_FILES["image"]["type"] == "image/png")) && ($_FILES["image"]["size"] < '4.194e+6'))
		{
			$filename = $_FILES['image']['name'];   
			$rand  = date('YmdHis');
			$img_name = $rand."_". $filename;
			move_uploaded_file($_FILES["image"]["tmp_name"], "../uploads/user/" . $img_name);
			
			if(isset($_POST['oldurl']))
			{
				$unlink_url = $uploadURL."/uploads/user/" . $oldurl;				
				unlink($unlink_url) ;
				//die;
			}
		}
		else
		{
			$_SESSION['emsg']="<div id='unsucc' class='error'>There was an error uploading image file.</div>";
			$_SESSION['emsg123'] ='yes';
			$Obj->Redirect($urlval);
			exit;
		}
	}
	
	
	$ValueArray = array($uname,$email,$img_name);
	$FieldArray = array('name','email','images');
	
	
	$Success = $Obj->Update_Dynamic_Query($ThisPageTable,$ValueArray,$FieldArray,'id',$Id);
	if($Success > 0)
	{
		$_SESSION['name']= $uname;
		$_SESSION['email'] = $email;
	
		$Msg = "<div style='color:green'>Profile updated successful</div>";  
		$_SESSION['emsg'] =$Msg;
		$_SESSION['emsg123'] ='yes';
	}
	else
	{
		$Msg = "Record not updated";  
		$_SESSION['emsg'] =$Msg;
		$_SESSION['emsg123'] ='yes';
	}
	
	header('location:profile_messages.php'); exit;
}
?>