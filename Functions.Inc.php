<?php 
require_once("Const.Inc.php") ;

class Func
{
	
	function paging($PageUrl,$PageSize,$TotalRecord,$Index)
	{
	    $TotalPage = ceil($TotalRecord / $PageSize) ;  
	    if($TotalPage > 1)
		{
?>
			<table width="98%" border="0" cellspacing="0" cellpadding="0">
			  <tr>
			    <td colspan="3">&nbsp;</td>
		      </tr>
			  <tr>
			    <td colspan="3"><table width="100%" border="0" cellspacing="0" cellpadding="0">
                  <tr>
                    <td width="50%" class="log"><?php echo "Total Records are " . $TotalRecord ; ?></td>
                    <td width="50%" class="log" align="right"><?php echo "Page " . $Index . " of Total " . $TotalPage . " Pages" ;?></td>
                  </tr>
                </table></td>
		      </tr>
			  <tr>
				<td colspan="3">&nbsp;</td>
			  </tr>
			  <tr>
			  
				<td width="19%">
				
				<?php if($Index > 1) { ?>
				
				<a href="<?php echo $PageUrl ;?>&Index=<?php echo $Index - 1 ; ?>" class="botlink">
				 Previous				</a>
				<?php } ?>				</td>
				
				<td width="63%" align="center">
				
				<?php 
				    for($i=1;$i<=$TotalPage;$i++)
					{
				?>
				        <a href="<?php echo $PageUrl ;?>&Index=<?php echo $i ; ?>" class="botlink">
						<?php 
							if($Index == $i)
							{
								echo "[ " ;
							}
								echo $i ; 
							if($Index == $i)
							{
								echo " ]" ;
							}
						?>
						</a>&nbsp;
				<?php
				    }
				?>				</td>
				
				
				<td width="18%" align="right">
				
				<?php if($Index < $TotalPage) { ?>
				
				<a href="<?php echo $PageUrl ;?>&Index=<?php echo $Index + 1 ; ?>" class="botlink">
				Next				</a>
				
				<?php } ?>				</td>
			  </tr>
			  <tr>
			    <td>&nbsp;</td>
			    <td>&nbsp;</td>
			    <td>&nbsp;</td>
		      </tr>
			</table>
<?php
		}
	}//Function paging End
	
function pagingevent($PageUrl,$PageSize,$TotalRecord,$Index)
	{
	    $TotalPage = ceil($TotalRecord / $PageSize) ;  
	    if($TotalPage > 1)
		{
?>
			<table width="98%" border="0" cellspacing="0" cellpadding="0">
			 
			
			  
			  <tr>
			  
				<td width="19%" class="links1">
				
				<?php if($Index > 1) { ?>
				
				<a href="<?php echo $PageUrl ;?>&Index=<?php echo $Index - 1 ; ?>" class="links1">
				 Previous				</a>
				<?php } ?>				</td>
				
				<td width="63%" align="center" class="links1">
				
				<?php /* //linking code 1 to end
				    for($i=1;$i<=$TotalPage;$i++)
					{
				?>
				        <a href="<?php echo $PageUrl ;?>&Index=<?php echo $i ; ?>" class="links1">
						<?php 
						if($Index==$i)
						{ 
						echo "[ "; 
						}
						echo $i ; ?>
						<?php 
						if($Index==$i)
						{ 
						echo "]"; 
						}
						?>
						</a>&nbsp;
				<?php
				    }*/
				?>	
				
	<?php				
$centerPages = "";
$sub1 = $Index - 1;
$sub2 = $Index - 2;
$add1 = $Index + 1;
$add2 = $Index + 2;
if ($Index == 1) {
     ?>
	<span class="links1">[<?php echo $Index ; ?>]</span>&nbsp;
     <a href="<?php echo $PageUrl ;?>&Index=<?php echo $add1 ; ?>" class="links1"><?php echo $add1 ; ?></a>&nbsp;
   <?php
} else if ($Index == $TotalPage) {
    ?>
    <a href="<?php echo $PageUrl ;?>&Index=<?php echo $sub1 ; ?>" class="links1"><?php echo $sub1 ; ?></a>&nbsp;
    <span class="links1">[<?php echo $Index ; ?>]</span>&nbsp;
    <?php
} else if ($Index > 2 && $Index < ($TotalPage - 1)) {
	?>
     <a href="<?php echo $PageUrl ;?>&Index=<?php echo $sub2 ; ?>" class="links1"><?php echo $sub2 ; ?></a>&nbsp;
    <a href="<?php echo $PageUrl ;?>&Index=<?php echo $sub1 ; ?>" class="links1"><?php echo $sub1 ; ?></a>&nbsp;
    <span class="links1">[<?php echo $Index ; ?>]</span>&nbsp;
    <a href="<?php echo $PageUrl ;?>&Index=<?php echo $add1 ; ?>" class="links1"><?php echo $add1 ; ?></a>&nbsp;
    <a href="<?php echo $PageUrl ;?>&Index=<?php echo $add2 ; ?>" class="links1"><?php echo $add2 ; ?></a>&nbsp;
    <?php
} else if ($Index > 1 && $Index < $TotalPage) {
	?>
	<a href="<?php echo $PageUrl ;?>&Index=<?php echo $sub1 ; ?>" class="links1"><?php echo $sub1 ; ?></a>&nbsp;
    <span class="links1">[<?php echo $Index ; ?>]</span>&nbsp;
    <a href="<?php echo $PageUrl ;?>&Index=<?php echo $add1 ; ?>" class="links1"><?php echo $add1 ; ?></a>&nbsp;
    <?php
}
				?>
				       
								</td>
				
				
				<td width="18%" align="right" class="links1">
				
				<?php if($Index < $TotalPage) { ?>
				
				<a href="<?php echo $PageUrl ;?>&Index=<?php echo $Index + 1 ; ?>" class="links1">
				Next				</a>
				
				<?php } ?>				</td>
			  </tr>
			  <tr>
			    <td>&nbsp;</td>
			    <td>&nbsp;</td>
			    <td>&nbsp;</td>
		      </tr>
			</table>
<?php
		}
	}//Function paging End
		
	
	
function pagingfront($PageUrl,$PageSize,$TotalRecord,$Index)
	{
		$TotalPage = ceil($TotalRecord / $PageSize) ;
	    if($TotalPage > 1)
		{
?>
			<table width="68%" border="0" cellspacing="0" cellpadding="0">
			  
			  <tr>
			  
				<td width="8%" class="link">
				
				<?php if($Index > 1) { ?>
				
				<a href="<?php echo $PageUrl ;?>&Index=<?php echo $Index - 1 ; ?>" class="link">
				 <strong style="font-size:14px;">Previous</strong>				</a>
				<?php } ?>				</td>
				
				<td width="83%" align="center" class="link">
				
				<?php 
				    for($i=1;$i<=$TotalPage;$i++)
					{
				?>
				        <a href="<?php echo $PageUrl ;?>&Index=<?php echo $i ; ?>" class="link">
						<span  style="font-size:14px;font-weight:bold;"><?php 
							if($Index == $i)
							{
								echo "[ " ;
							}
								echo $i ; 
							if($Index == $i)
							{
								echo " ]" ;
							}
						?>
						</span>
						</a>&nbsp;
				<?php
				    }
				?>				</td>
				
				
				<td width="8%" align="right" class="link">
				
				<?php if($Index < $TotalPage) { ?>
				
				<a href="<?php echo $PageUrl ;?>&Index=<?php echo $Index + 1 ; ?>" class="link">
				<strong style="font-size:14px;">Next</strong>				</a>
				
				<?php } ?>				</td>
			  </tr>
			  <tr>
			    <td>&nbsp;</td>
			    <td>&nbsp;</td>
			    <td>&nbsp;</td>
		      </tr>
			</table>
<?php
		}
	}//Function paging End
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	function paging_photo($PageUrl,$PageSize,$TotalRecord,$Index)
	{
	    $TotalPage = ceil($TotalRecord / $PageSize) ;  
	    if($TotalPage > 1)
		{
?>
			<table width="98%" border="0" cellspacing="0" cellpadding="0">
	
			  
				<td width="19%" align="left">
				
				<?php if($Index > 1) { ?>
				
				<a href="<?php echo $PageUrl ;?>&Index=<?php echo $Index - 1 ; ?>" class="botlink" style="text-decoration:none">
				 Previous			</a>
				<?php } ?>				</td>
				
				
				
				
				
				<td width="70%" align="right">
				
				<?php 
				    for($i=1;$i<=$TotalPage;$i++)
					{
				?>
				        <a href="<?php echo $PageUrl ;?>&Index=<?php echo $i ; ?>" class="botlink" style="text-decoration:none">
					<?php 
							if($Index == $i)
							{
								echo "<span style=\"color:#000; font-weight:bold\">";
							}
								echo $i ; 
							if($Index == $i)
							{
								echo " </span>" ;
							}
						?>
						</a>&nbsp;
				<?php
				    }
				?>				</td>
				
				
				
				
				<td width="7%" align="left">
				
				<?php if($Index < $TotalPage) { ?>
				
				<a href="<?php echo $PageUrl ;?>&Index=<?php echo $Index + 1 ; ?>" class="botlink" style="text-decoration:none">
				Next	</a>
				
				<?php } ?>				</td>
			  </tr>
			 
			</table>
<?php
		}
	}//Function paging End
	
	
	
	
	
	
	
	function paging_video($PageUrl,$PageSize,$TotalRecord,$Index)
	{
	    $TotalPage = ceil($TotalRecord / $PageSize) ;  
	    if($TotalPage > 1)
		{
?>
			<table width="98%" border="0" cellspacing="0" cellpadding="0">
	
			  
				<td width="19%" align="left">
				
				<?php if($Index > 1) { ?>
				
				<a href="<?php echo $PageUrl ;?>&Index=<?php echo $Index - 1 ; ?>" class="txt" style="text-decoration:none">
				 Previous			</a>
				<?php } ?>				</td>
				
				
				
				
				
				<td width="70%" align="right">
				
				<?php 
				    for($i=1;$i<=$TotalPage;$i++)
					{
				?>
				        <a href="<?php echo $PageUrl ;?>&Index=<?php echo $i ; ?>" class="txt" style="text-decoration:none">
					<?php 
							if($Index == $i)
							{
								echo "<span style=\"color:#000; font-weight:bold\">";
							}
								echo $i ; 
							if($Index == $i)
							{
								echo " </span>" ;
							}
						?>
						</a>&nbsp;
				<?php
				    }
				?>				</td>
				
				
				
				
				<td width="7%" align="left">
				
				<?php if($Index < $TotalPage) { ?>
				
				<a href="<?php echo $PageUrl ;?>&Index=<?php echo $Index + 1 ; ?>" class="txt" style="text-decoration:none">
				Next	</a>
				
				<?php } ?>				</td>
			  </tr>
			 
			</table>
<?php
		}
	}//Function paging End
	
	
	
	
		function paging_photo1($PageUrl,$PageSize,$TotalRecord,$Index)
	{
	    $TotalPage = ceil($TotalRecord / $PageSize) ;  
	    if($TotalPage > 1)
		{
?>
			<table width="98%" border="0" cellspacing="0" cellpadding="0">
	
			  
				<td width="19%" align="left">
				
				<?php if($Index > 1) { ?>
				
				<a href="<?php echo $PageUrl ;?>&Index=<?php echo $Index - 1 ; ?>" class="txt" style="text-decoration:none">
				 Previous			</a>
				<?php } ?>				</td>
				
				
				
				
				
				<td width="70%" align="right">
				
				<?php 
				    for($i=1;$i<=$TotalPage;$i++)
					{
				?>
				        <a href="<?php echo $PageUrl ;?>&Index=<?php echo $i ; ?>" class="txt" style="text-decoration:none">
					<?php 
							if($Index == $i)
							{
								echo "<span style=\"color:#000; font-weight:bold\">";
							}
								//echo $i ; 
							if($Index == $i)
							{
								echo " </span>" ;
							}
						?>
						</a>&nbsp;
				<?php
				    }
				?>				</td>
				
				
				
				
				<td width="7%" align="left">
				
				<?php if($Index < $TotalPage) { ?>
				
				<a href="<?php echo $PageUrl ;?>&Index=<?php echo $Index + 1 ; ?>" class="txt" style="text-decoration:none">
				Next	</a>
				
				<?php } ?>				</td>
			  </tr>
			 
			</table>
<?php
		}
	}//Function paging End
	
	
	
	
	function paging_video1($PageUrl,$PageSize,$TotalRecord,$Index)
	{
	    $TotalPage = ceil($TotalRecord / $PageSize) ;  
	    if($TotalPage > 1)
		{
?>
			<table width="98%" border="0" cellspacing="0" cellpadding="0">
	
			  
				<td width="19%" align="left">
				
				<?php if($Index > 1) { ?>
				
				<a href="<?php echo $PageUrl ;?>&Index=<?php echo $Index - 1 ; ?>" class="txt" style="text-decoration:none">
				 Previous			</a>
				<?php } ?>				</td>
				
				
				
				
				
				<td width="70%" align="right">
				
				<?php 
				    for($i=1;$i<=$TotalPage;$i++)
					{
				?>
				        <a href="<?php echo $PageUrl ;?>&Index=<?php echo $i ; ?>" class="txt" style="text-decoration:none">
					<?php 
							if($Index == $i)
							{
								echo "<span style=\"color:#000; font-weight:bold\">";
							}
								//echo $i ; 
							if($Index == $i)
							{
								echo " </span>" ;
							}
						?>
						</a>&nbsp;
				<?php
				    }
				?>				</td>
				
				
				
				
				<td width="7%" align="left">
				
				<?php if($Index < $TotalPage) { ?>
				
				<a href="<?php echo $PageUrl ;?>&Index=<?php echo $Index + 1 ; ?>" class="txt" style="text-decoration:none">
				Next	</a>
				
				<?php } ?>				</td>
			  </tr>
			 
			</table>
<?php
		}
	}//Function paging End
	
	
	
	
	
	
	
	function pagingdiscount($PageUrl,$PageSize,$TotalRecord,$Index)
	{
	    $TotalPage = ceil($TotalRecord / $PageSize) ;  
	    if($TotalPage > 1)
		{
?>
			<table width="98%" border="0" cellspacing="0" cellpadding="0">
			  <tr>
			    <td colspan="3">&nbsp;</td>
		      </tr>
			  <tr>
			    <td colspan="3"><table width="100%" border="0" cellspacing="0" cellpadding="0">
                  
                </table></td>
		      </tr>
			  
			  <tr>
			  
				
                <td width="19%" align="left" bgcolor="#FFFFFF" class="linkyellow">
				
				<?php if($Index > 1) { ?>
				
				<a href="<?php echo $PageUrl ;?>&Index=<?php echo $Index - 1 ; ?>" bgcolor="#FFFFFF" class="linkyellow" style="text-decoration:none">
				 Previous			</a>
				<?php } ?>				</td>
				
				<td width="63%" align="center">
				
				<?php 
				    for($i=1;$i<=$TotalPage;$i++)
					{
				?>
				        <a href="<?php echo $PageUrl ;?>&Index=<?php echo $i ; ?>" bgcolor="#FFFFFF" class="linkyellow">
						<?php 
							if($Index == $i)
							{
								echo "<span style='color:#063'>" ;
							}
								echo $i ; 
							if($Index == $i)
							{
								echo "</span>" ;
							}
						?>
						</a>
				<?php
				    }
				?>				
                </td>
				
				
				<td width="7%" align="left">
				
				<?php if($Index < $TotalPage) { ?>
				
				<a href="<?php echo $PageUrl ;?>&Index=<?php echo $Index + 1 ; ?>" bgcolor="#FFFFFF" class="linkyellow" style="text-decoration:none">
				Next	</a>
				
				<?php } ?></td>
			  </tr>
			  <tr>
			   <td>&nbsp;</td>
		      </tr>
			</table>
<?php
		}
	}
	
	
}//Class Terminator

?>
