<?php 
include("Query.Inc.php") ;
$Obj = new Query($DBName) ;

$Id = $_SESSION['UserID'];

$ThisPageTable = 'users';
$DataArray = $Obj->Select_Dynamic_Query($ThisPageTable,'',array('id'),array('='),array($Id)) ;
$name = $DataArray[0]['name'] ;
$email = $DataArray[0]['email'] ;
$images = $DataArray[0]['images'] ;
$fb_profile_pic_url = $DataArray[0]['fb_profile_pic_url'] ;

?>
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<title>Edit Profile - Nexzest</title>
<link rel="stylesheet" href="css/style.css" />
</head>

<body>
<div id="wrapper">
    <?php require_once "header.php"; ?>
    <section class="dashboard_main_container">
    	
        
     <section class="dashboard_main_container">
    	<div class="edit_profile_container">
	<img src="img/top-close_btn.png" class="top_close_btn" />
    
    <header> EDIT PROFILE</header>
    <form id="editForm" name="editForm"  method="post" action="editprofile_action.php" enctype="multipart/form-data">
    <input name="uname" type="text" id="uname" value="<?php echo $name; ?>" placeholder="Full Name" class="text_field"  />
  <input name="email" type="text" id="email" value="<?php echo $email; ?>" class="text_field" readonly/>
    
  <hr>
    
  <input name="image" type="file"  id="image" style="margin-left: 30px;" />
  <div class="profile_pic_display">
  <?php 
					if($images != '')
                    { ?>
                    <img src="<?php echo $CouponSiteUrl; ?>/uploads/user/<?php echo $images; ?>" />
                    <?php } else if($fb_profile_pic_url != '') { ?>
                    <img src="<?php echo $fb_profile_pic_url; ?>" />
                    <?php } else { ?>
    	<img src="img/prof_pic.jpg" />
        
        <?php } ?>
    </div>
    
     <hr>
     <div style="width:100%; float:left; height:auto;">
     <img src="img/update_btn.png" onClick="$('#editForm').submit()" class="update_btn" />
     <img src="img/close_btn.png" onClick="window.location='profile.php'" style="cursor:pointer;" class="close_btn" />
     </div>
     <div style="width:100%; float:left; height:auto;">
         <div class="reset_password"  onClick="window.location='reset_password.php'"  style="cursor:pointer;">Reset Password</div>
         <div class="seperator"><img src="img/seperator.png" /></div>
         <div class="delete_account" onClick="confirmDelete();"  style="cursor:pointer;">Delete Account</div>
     </div>
    </form>
</div>
            
    <div style="clear:both;"></div>
    
    </section><!--left_container-->   
        
       <div class="back"> <a href="index.php"><img src="img/back.png" /></a> </div>
     
    <div style="clear:both;"></div>
    </section>    
    <?php require_once "footer.php"; ?>    
</div>
</body>
</html>
<?php
	require_once "scripts.php";
?>

<script type="text/javascript">
confirmDelete = function() {
	var postval= "<?php echo base64_decode($_SESSION['user_password']);?>";
	var person = prompt("Please enter your password.");
	if (person!=null)
	  {
		if (person == postval)
		{
			if(confirm('Are you sure you want to delete?'))
			{	
				window.location.href = "delete_account.php";
			}
		}
		else
		{
			alert(" The password you entered is not valid. Please try again.");
			delete_account();
		}	
	  }
	
}
</script>