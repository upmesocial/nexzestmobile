<?php

include("Query.Inc.php") ;
include("Functions.Inc.php") ;

?>
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<title>Reset Password - Nexzest</title>
<link rel="stylesheet" href="css/style.css" />
</head>

<body>
<div id="wrapper">
    <?php require_once "header.php"; ?>
    <section class="forget_password_container">
    	<form autocomplete="off" id="resetpswdForm" name="resetpswdForm"  method="post" action="reset_password_action.php"> 
        <div class="forget_password_box">
       
        	<header>
            Reset Password
            </header>
            <input style="padding:2px; color:#666;" class="text_field" name="old_pswd" type="password" id="old_pswd" value="" placeholder="Current Password"  autocomplete='off'/>
            <input name="password" type="password" id="password" placeholder="New Password" class="text_field" />
            <input name="conf_password"  type="password" id="conf_password"  placeholder="Confirm New Password" class="text_field" />
        
        <input class="submit_sub" type="submit" name="edit_profile" value="Update" >
        
       
     	</div>
        </form>
        <div class="back"> <a href="index.php"><img src="img/back.png" /></a> </div>
        
      <div style="clear:both;"></div>
    </section>
    <?php require_once "footer.php"; ?>
</div>
</body>
</html>
<?php
	require_once "scripts.php";
?>