<?php
include("Query.Inc.php");

?>
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<title>Loign - Nexzest</title>
<link rel="stylesheet" href="css/style.css" />
</head>

<body>
<div id="wrapper">
    <header class="main_header anonymos">
    	<a href="index.php"><img src="img/logo.png" class="logo"  style="border:none;" /></a>
        <div class="login_btn">
       		<a href="login.php"><img src="img/login.png" style="border:none;" /></a>
        </div>
        <div style="clear:both;"></div>
    </header>
    <section class="login_main_container">
        <div class="fb_login_container">
        <h1>Login Using Facebook</h1>	
            
          <p> Email: </p>
            <input disabled="disabled" name="username" type="text" value="<?php if(isset($_SESSION['verify_email']) && $_SESSION['verify_email']!=''){echo $_SESSION['verify_email'];}?>" class="text_field"/><br />
          <p> Password: </p>
        	<input class="text_field" disabled="disabled" name="password" type="password" value="<?php if(isset($_SESSION['verify_password']) && $_SESSION['verify_password']!=''){echo $_SESSION['verify_password'];}?>" />
              <br />                     
          
          
          
          <div class="fb_banner"  onClick="fblogin();" >
      <b>Login with Facebook</b>
        </div>
          <div style="clear:both;"></div>
        </div>
        
        <div class="back"> <a href="index.php"><img src="img/back.png" /></a> </div>
    <div style="clear:both;"></div>
    </section>
    <div class="top_footer">
    	<h3>CONNECTING YOU WITH LOCAL BUSINESSES</h3>
    	<p>Call Us Now: 267-272-1326  |  EMAIL: <span class="yellow_font"><a href="mailto:info@nexzest.com">INFO@NEXZEST.COM</a></span></p>	
    </div>
    <footer>
    	Copyright 2013 Nexzest. All Rights Reserved  | <br /> Call Us Now: 267-272-1326 
        <div class="social_icons">
        <a href="#"><img src="img/tw.png" /></a>
        <a href="#"><img src="img/yt.png" /></a>
        <a href="#"><img src="img/fb.png" /></a>
        <a href="#"><img src="img/in.png" /></a>
        <a href="#"><img src="img/p.png" /></a>
        <a href="#"><img src="img/c.png" /></a>
         </div>
     <div class="patent_pending"> Patent Pending </div>
    <div style="clear:both;"></div> 
    </footer>
</div>
</body>
</html>
<?php
	require_once "scripts.php";
?>
<script src="http://connect.facebook.net/en_US/all.js"></script>
<script>
window.fbAsyncInit = function() {
    FB.init({
       appId: '348269895312107',
       cookie: true,
       xfbml: true,
       oauth: true
    }); 
};

fblogin = function() {

	FB.login(function(response){
		if (response.authResponse) {
			window.location='facebook/validatefb.php';
		}
		
	},{scope: 'publish_stream, email, user_about_me, user_birthday, user_education_history, user_hometown, user_likes, user_location, user_photos, user_status, user_website, user_work_history'});	
	
}
</script>
