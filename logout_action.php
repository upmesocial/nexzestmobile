<?php
include("Query.Inc.php");

$obj = new Query($DBName);
if ($obj->isValidSession()) {
 // $obj->Redirect('dashboard.php');
}

?>
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<title>Dashboard - Nexzest</title>
<link rel="stylesheet" href="css/style.css" />
</head>
<body>
<?php if ($obj->isValidSession()) { ?>
    <div id="wrapper">
        <?php require_once "header.php"; ?>
        <section class="logout_action_main_container">
    	   <div class="logout_action_container2">
	
    
            <!-- <header><span style="color:#000;"><strong> NOTE:</strong></span> Please make sure that you are signed out of your <strong><u>facebook</u></strong> account before logout your nexzest account.</header> -->
            <br /><br /><br /> <br />
            <!--<form name="resendForm" id="resendForm" action="" method="post">                
                <input class="logout_resend_sub" type="submit" name="resend" value="" >
            </form>-->
   							
   						
         <a href="logout.php"><img src="img/log-out-pop.png"></a>
   							 <br />
   							<br /><br /><br />
    						<a href="logout.php?action=user"><img src="img/log-in-pop.png"></a>
    
    </div>
<?php } else { ?>
    <div id="wrapper">
        <?php require_once "header.php"; ?>
        <section class="logout_action_main_container">
           <div class="logout_action_container2">
    
    
            <!-- <header><span style="color:#000;"><strong> NOTE:</strong></span> Please make sure that you are signed out of your <strong><u>facebook</u></strong> account before logout your nexzest account.</header> -->
            <br /><br /><br />
           <!-- <form name="resendForm" id="resendForm" action="" method="post">                
                <input class="logout_resend_sub" type="submit" name="resend" value="" >
            </form>-->
            <a id="sendEmail" href="send_email.php"><img src="img/resend_link.png"></a>   
            <br />
            <br />           
                        
            <a href="logout.php"><img src="img/log-out-pop.png"></a>
                             <br />
                            <br />
                            <a href="logout.php?action=user"><img src="img/log-in-pop.png"></a>
    
    </div>
<?php } ?>
      <div class="back"> <a href="index.php"><img src="img/back.png" /></a> </div>      
    <div style="clear:both;"></div>
    </section>    
    <?php require_once "footer.php"; ?>    
</div>
</body>
</html>
      <?php
       /* if(isset($_POST['resend']))
            {
                $newid= base64_encode($obj->GetUserId($_SESSION['email']));
                $to = $_SESSION['email']; 
                //echo $newid;
                $subject1= $SITENAME." Registration Confirmation"; 
                $headers1 = "MIME-Version: 1.0" . "\r\n"; 
                $headers1 .= "Content-type:text/html;charset=iso-8859-1" . "\r\n"; 
                $headers1.="From: ".$SITENAME." <donot-reply@nexzest.com>";
                $message1 = "";
                $message1="You have been successfully registered at ".$SITENAME.". <br/>";
                //$message1.="<img src='http://localhost/nexzest/img/logo.png' alt='' border='none' /> ";
                $message1.="Click on this link to login to your account <br/>";
                $message1.="<a href='".SITEROOT."/verify.php?val=".$newid."'>Click Here To Verify</a>" ;
                $message = file_get_contents('mails.html');
                $message = str_replace('[[sitename]]', $SITENAME, $message);
                $message = str_replace('[[siteroot]]', SITEROOT, $message);
                $message = str_replace('[[msgdata]]', $message1, $message); 
                
                if(mail($to,$subject1,$message,$headers1)) { 
                    //echo "E-Mail Sent"; 
                    $_SESSION['msg'] = 'Set';
                } else { 
                    //echo "There was a problem"; 
                }  
               // $file= mail($to,$subject1,$message,$headers1);

            }*/
    ?>

    <?php 
        if(isset($_SESSION['msg']) and $_SESSION['msg']=='Set'){ ?>
            <div class="popup-outer" style="display:block;">
            <div  class="popup-bg"></div>
            <div class="popup-container"><div class="bubble_container">
            <img src="img/close_btn_1.jpg" width="53" height="18" style="cursor:pointer;" onClick="$('.popup-outer').css('display','none')" />
            <div style="clear:both;"></div>
            <p>Another activation link has been sent to your Email.</p>

        </div></div>
        </div>
    <?php 
        unset($_SESSION['msg']);
     }?>
<?php
	require_once "scripts.php";
?>