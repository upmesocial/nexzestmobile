<?php
ob_start();
session_start();
session_destroy();
//unset($_SESSION['non_verfied']);
$action = (isset($_REQUEST['action']) && !empty($_REQUEST['action'])) ? $_REQUEST['action'] : "";

if(!empty($action) && 'user' == $action) {
	header('location:login.php');
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<title>Loign - Nexzest</title>
<link rel="stylesheet" href="css/style.css" />
</head>

<body>
<div id="wrapper">
    <header class="main_header anonymos">
    	<a href="index.php"><img src="img/logo.png" class="logo" style="border:none;" /></a>
        <div class="login_btn">
       		<a href="login.php"><img src="img/login.png" style="border:none;" /></a>
        </div>
        <div style="clear:both;"></div>
    </header>
    <section class="login_main_container">
       <div class="logout_msg_box">
        	
           <div>You have successfully logged out of Nexzest. Thank you for using Nexzest.</div>
          
        </div>
        
        <div class="back"><a href="index.php"><img src="img/back.png" /></a> </div>
    <div style="clear:both;"></div>
    </section>
    <div class="top_footer">
    	<h3>CONNECTING YOU WITH LOCAL BUSINESSES</h3>
    	<p>Call Us Now: 267-272-1326  |  EMAIL: <span class="yellow_font"><a href="mailto:info@nexzest.com">INFO@NEXZEST.COM</a></span></p>	
    </div>
    <footer>
    	Copyright 2013 Nexzest. All Rights Reserved  | <br /> Call Us Now: 267-272-1326 
        <div class="social_icons">
        <a href="#"><img src="img/tw.png" /></a>
        <a href="#"><img src="img/yt.png" /></a>
        <a href="#"><img src="img/fb.png" /></a>
        <a href="#"><img src="img/in.png" /></a>
        <a href="#"><img src="img/p.png" /></a>
        <a href="#"><img src="img/c.png" /></a>
         </div>
     <div class="patent_pending"> Patent Pending </div>
    <div style="clear:both;"></div> 
    </footer>
</div>
</body>
</html>
<?php
	require_once "scripts.php";
?>
