<?php
include("Query.Inc.php");

$obj = new Query($DBName);
if ($obj->isValidSession()) {
	$obj->Redirect('dashboard.php');
}

?>

<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<title>Register - Nexzest</title>
<link rel="stylesheet" href="css/style.css" />
</head>

<body>
<div id="wrapper">
    <header class="main_header anonymos">
    	<a href="index.php"><img src="img/logo.png" class="logo" style="border:none;" /></a>
        <div class="login_btn">
       		<a href="login.php"><img src="img/login.png" style="border:none;" /></a>
        </div>
        <div style="clear:both;"></div>
    </header>
    <section class="login_main_container">
        <div class="registration_msg_box">
        	<?php if (isset($_SESSION['msg']) && $_SESSION['msg'] == 'not_verify') { ?>
                 <div>Your email address has not been verified yet. Please verify your email address to login in nexzest account.<br><br>
                    Note:Check your spam folder if you cant locate email.
                 </div>
            <?php } else { ?>
                 <div>Your account has been made. Please verify it by clicking the activation link that has been sent to your email. If activation email isn't in your inbox, please check your spam folder in your email. Thank you. </div>
            <?php } ?>
          
          <form name="resendForm" id="resendForm" action="" method="post">                
           <input class="resend_sub" type="submit" name="resend" value="Resend Confirmation Email" >
          </form>
        </div>
        
        <!--<div class="back"><a href="index.php"><img src="img/back.png" /></a> </div>-->
    <div style="clear:both;"></div>
    </section>
    <?php
        if(isset($_POST['resend']))
            {
                $newid= base64_encode($obj->GetUserId($_SESSION['email']));
                $to = $_SESSION['email']; 
                //echo $newid;
                $subject1= $SITENAME." Registration Confirmation"; 
                $headers1 = "MIME-Version: 1.0" . "\r\n"; 
                $headers1 .= "Content-type:text/html;charset=iso-8859-1" . "\r\n"; 
                $headers1.="From: ".$SITENAME." <donot-reply@nexzest.com>";
                $message1 = "";
                $message1="You have been successfully registered at ".$SITENAME.". <br/>";
                //$message1.="<img src='http://localhost/nexzest/img/logo.png' alt='' border='none' /> ";
                $message1.="Click on this link to login to your account <br/>";
                $message1.="<a href='".SITEROOT."/verify.php?val=".$newid."'>Click Here To Verify</a>" ;
                $message = file_get_contents('mails.html');
                $message = str_replace('[[sitename]]', $SITENAME, $message);
                $message = str_replace('[[siteroot]]', SITEROOT, $message);
                $message = str_replace('[[msgdata]]', $message1, $message); 
                
                if(mail($to,$subject1,$message,$headers1)) { 
                    //echo "E-Mail Sent"; 
                    $_SESSION['msg'] = 'Set';
                } else { 
                    //echo "There was a problem"; 
                }  
               // $file= mail($to,$subject1,$message,$headers1);

            }
    ?>

    <?php 
        if(isset($_SESSION['msg']) and $_SESSION['msg']=='Set'){ ?>
            <div class="popup-outer" style="display:block;">
            <div  class="popup-bg"></div>
            <div class="popup-container"><div class="bubble_container">
            <img src="img/close_btn_1.jpg" width="53" height="18" style="cursor:pointer;" onClick="$('.popup-outer').css('display','none')" />
            <div style="clear:both;"></div>
            <p>Another activation link has been sent to your Email.</p>

        </div></div>
        </div>
    <?php 
        unset($_SESSION['msg']);
     }?>


    <div class="top_footer">
    	<h3>CONNECTING YOU WITH LOCAL BUSINESSES</h3>
    	<p>Call Us Now: 267-272-1326  |  EMAIL: <span class="yellow_font"><a href="mailto:info@nexzest.com">INFO@NEXZEST.COM</a></span></p>	
    </div>
    <footer>
    	Copyright 2013 Nexzest. All Rights Reserved  | <br /> Call Us Now: 267-272-1326 
        <div class="social_icons">
        <a href="#"><img src="img/tw.png" /></a>
        <a href="#"><img src="img/yt.png" /></a>
        <a href="#"><img src="img/fb.png" /></a>
        <a href="#"><img src="img/in.png" /></a>
        <a href="#"><img src="img/p.png" /></a>
        <a href="#"><img src="img/c.png" /></a>
         </div>
     <div class="patent_pending"> Patent Pending </div>
    <div style="clear:both;"></div> 
    </footer>
</div>
</body>
<script type="text/javascript">
    function confimrResend() {
        alert('Activation link has been sent to your Email.');
        var overlay = document.getElementById("overlay");
      //Set a variable to contain the DOM element of the popup
      var popup = document.getElementById("popup");

      //Changing the display css style from none to block will make it visible
      overlay.style.display = "block";
      //Same goes for the popup
      popup.style.display = "block";
    }
</script>
</html>
<?php
	require_once "scripts.php";
?>